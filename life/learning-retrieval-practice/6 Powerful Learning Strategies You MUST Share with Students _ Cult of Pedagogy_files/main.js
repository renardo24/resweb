$(document).ready(function(){

    // search bar

    $('.search-icon').click( function(){
      $('body').addClass('search-active');
      $('.input-search').focus();
    });

    $('.close-icon').click( function(){
      $('body').removeClass('search-active');
    });

    // responsive navigation

    $('.nav-main').meanmenu( {
        meanMenuContainer: '.header-main',
        meanScreenWidth: "980",
        meanMenuClose: "close",
        meanMenuOpen: "menu"
    });

    // swap the image if a dark header is chosen

    $(".page-color-charcoal .header-main .logo-small").attr("src","https://www.cultofpedagogy.com/wp-content/themes/cult-of-pedagogy/images/cult-of-pedagogy-logo-white.svg");

    // show/hide
    $('.toggle').on('click', function(e) {
      e.preventDefault();
      $(this).toggleClass("active");
      $(this).next().slideToggle(100);
    });

});
