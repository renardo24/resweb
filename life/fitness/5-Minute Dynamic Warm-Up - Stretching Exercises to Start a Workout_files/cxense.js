tp.define(['jq', 'util', 'recWidgetService'],
  function ($, util, recWidgetService) {
    var cX;
    var recWidget = recWidgetService.__protected__.getInstance();

    var _loadCxLibrary = function () {
      var logError = function (libraryName) {
        return function () {
          util.error('cxense: ' + libraryName + ' library was not loaded');
        }
      };

      if (!_isCxenseConfigured()) {
        recWidgetService.__protected__.insertWidgetLib({
          src: _getRootEndpoint() + '/cx.cce.js',
          defer: true,
          onerror: logError('cx.cce')
        });

        recWidgetService.__protected__.insertWidgetLib({
          src: _getRootEndpoint() + '/cx.js',
          defer: true,
          onerror: logError('cx')
        });
      }

      cX = window.cX = window.cX || {};
      cX.callQueue = cX.callQueue || [];
      cX.CCE = cX.CCE || {};
      cX.CCE.callQueue = cX.CCE.callQueue || [];
    };

    var _init = function (config) {
      if (!config.siteId) {
        return;
      }

      util.debug('cxense: configure siteId and pageviewId within Cxense: ', config);

      _loadCxLibrary();
      
      invokeCxenseFn(function () {
        cX.setSiteId(config.siteId);
        cX.setRandomId(config.pageviewId);
      });
    };

    var _getRootEndpoint = function () {
      var rootEndpoint = tp.cxenseCdnEndpoint
        ? tp.cxenseCdnEndpoint
        : '//cdn.cxense.com';

      return rootEndpoint;
    };

    var _isCxenseConfigured = function () {
      return (typeof window.cX !== "undefined") && (typeof window.cX.CCE !== "undefined");
    };

    var _isCustomerPrefixConfigured = function (config) {
      return (typeof config.cxenseCustomerPrefix !== "undefined");
    };

    var _sendPageviewEvent = function (config) {
      if (!_isCxenseConfigured() || !_isCustomerPrefixConfigured(config)) {
        util.debug('cxense: is not configured or pageview collection is disabled');
        return;
      }

      util.debug('cxense: sending pageview with config: ', config);

      invokeCxenseFn(function () {
        cX.addExternalId({ id: config.browserId, type: config.cxenseCustomerPrefix });
        cX.CCE.sendPageViewEvent();
      });
    }

    var _showRecommendation = function (widgetConfig) {
      if (!_isCxenseConfigured()) {
        util.debug('cxense: is not configured');
        return;
      }

      recWidget.showRecommendation({
        widgetConfig: widgetConfig,
        createPlaceholderFn: getPlaceholderFn(widgetConfig.widgetId, widgetConfig.displayMode)
      });

      invokeCxenseFn(function () {
        cX.CCE.run({
          widgetId: widgetConfig.widgetId,
          targetElementId: 'cxense-' + widgetConfig.widgetId
        }, null, function (cxenseHttpResponse) {
          if (
              widgetConfig.displayMode === 'modal'
              && cxenseHttpResponse && cxenseHttpResponse.response
              && !cxenseHttpResponse.response.error
            ) {
              recWidget.fixWidgetModalWidth(
                recWidget.findWidget(widgetConfig.widgetId)
              );
          }
        });
      });
    }

    var getPlaceholderFn = function (widgetId, displayMode) {
      return function () {
        return $('<div class="tp-cxense-placeholder-' + displayMode + ' tp-widget-placeholder">' + 
          '<div id="cxense-' + widgetId + '"></div></div>')
      };
    }

    var invokeCxenseFn = function (fn) {
      cX.CCE.callQueue.push(['invoke', fn]);
    }

    var removeWidgetsAndSdk = function () {
      recWidget.clearWidgets();
      delete window.cX;
    };

    return {
      name: 'cxense',
      __protected__: {
        init: _init,
        showRecommendation: _showRecommendation,
        sendPageviewEvent: _sendPageviewEvent
      },
      __private__: {
        removeWidgetsAndSdk: removeWidgetsAndSdk
      }
    };
  }
);
