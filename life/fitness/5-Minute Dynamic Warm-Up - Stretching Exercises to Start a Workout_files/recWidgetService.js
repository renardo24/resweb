tp.define(['jq', 'util', 'containerUtils'],
  function ($, util, containerUtils) {
    var isInline = function (config) {
      return config.displayMode === 'inline';
    };

    var isModal = function (config) {
      return config.displayMode === 'modal';
    };

    var configureWidget = function (widgetConfig) {
      var wrapper = $(widgetConfig.containerSelector).first();
      var uuid = 'widget-id-' + util.randomString();
      var config = {
        singleContainer: wrapper.length ? wrapper[0] : undefined,
        displayMode: widgetConfig.displayMode || 'modal',
        showCloseButton: widgetConfig.showCloseButton,
        iframeParams: {
          iframeId: uuid,
          containerSelector: widgetConfig.containerSelector,
          width: widgetConfig.width,
          height: widgetConfig.height
        }
      };

      return {
        uuid: uuid,
        widgetId: widgetConfig.widgetId,
        placeholder: widgetConfig.placeholder,
        trackingId: widgetConfig.trackingId,
        config: config
      };
    };

    var getWidgetContainer = function (iframeConfig, closeCallback) {
      var container;

      if (isInline(iframeConfig)) {
        var inlineContainer = containerUtils.initInlineContainer(iframeConfig);

        container = inlineContainer.container;
      } else if (isModal(iframeConfig)) {
        container = containerUtils.getModalContainer(iframeConfig, function () {
          $.isFunction(closeCallback) && closeCallback();
        });
      }

      return container;
    };

    var setWidgetCallbacks = function (widget, callbacks) {
      $.each(callbacks, function (callbackName, getCallback) {
        if ($.isFunction(getCallback)) {
          widget[callbackName] = getCallback(widget);
        }
      });
    };

    var _recWidget = function () {
      var widgets = [];
      var modalWidget = null;

      var _showRecommendation = function (recConfig) {
        var widgetConfig = recConfig.widgetConfig || {};
        var createPlaceholderFn = recConfig.createPlaceholderFn;
        var widgetId = widgetConfig.widgetId;

        if (!widgetId) {
          throw new Error('widgetId should be specified');
        }

        if (_findWidget(widgetId)) {
          util.log('widget with id ' + widgetConfig.widgetId + ' already initialize');
          return;
        }

        if (isModal(widgetConfig) && modalWidget) {
          util.log('modal widget already shown');
          return;
        }

        var widget = configureWidget(widgetConfig);
        var iframeConfig = widget.config;
        var element = createPlaceholderFn();

        widget.container = getWidgetContainer(iframeConfig, function () {
          _closeWidget(widget);
        });
        widget.element = element.appendTo(widget.container);

        if (isModal(iframeConfig)) {
          modalWidget = widget;

          if (util.isIphone() || util.isIOsUiWebView() || util.isAndroid()) {
            containerUtils.__private__.iphoneFix.modalOpened();
          }
        }

        setWidgetCallbacks(widget, recConfig.widgetCallbacks);

        widgets.push(widget);

        return widget;
      };

      var _fireWidgetCallback = function (eventName, widgetId, eventData) {
        widgets.forEach(function (widget) {
          if (widget.widgetId !== widgetId.toString()) {
            return;
          }

          var eventCallback = widget['on' + eventName];
          
          if ($.isFunction(eventCallback)) {
            eventCallback(eventData);
          }
        });
      };

      var _closeWidget = function (widget) {
        if (isInline(widget.config)) {
          widget.container.empty();
        } else if (isModal(widget.config)) {
          _closeModalContainer(widget);
          widget.container.detach();
        }

        if ($.isFunction(widget.onclose)) {
          widget.onclose();
        }

        widgets = widgets.filter(function (_widget) {
          return _widget.uuid !== widget.uuid;
        });
      };

      var _clearWidgets = function () {
        widgets = [];
        modalWidget = null;
      };

      var _findWidget = function (widgetId) {
        return widgets.filter(function (widget) {
          return widgetId === widget.widgetId;
        })[0];
      };

      var _closeModalContainer = function () {
        if (!modalWidget) {
          return;
        }

        containerUtils.closeModal(modalWidget);
        modalWidget.container.empty();

        var iphoneFix = containerUtils.__private__.iphoneFix;

        if (iphoneFix.isIphoneCaretFixed) {
          iphoneFix.modalClosed();
        }

        modalWidget = null;
      };

      var _fixWidgetModalWidth = function (widget, hasIframe) {
        if (widget && !isModal(widget.config)) {
          return;
        }
  
        var container = widget.container;
        var widgetPlaceholder = hasIframe
          ? container.find('iframe')
          : container.find('.tp-widget-placeholder');
  
        containerUtils.checkBackdropAndContainer(container, true, true);
  
        setTimeout(function () {
          containerUtils.__protected__.setElementSizes(container, {
            width: widgetPlaceholder.width()
          });
        }, 100);
      };

      return {
        showRecommendation: _showRecommendation,
        fireWidgetCallback: _fireWidgetCallback,
        findWidget: _findWidget,
        clearWidgets: _clearWidgets,
        closeWidget: _closeWidget,
        fixWidgetModalWidth: _fixWidgetModalWidth
      };
    };

    var _insertWidgetLib = function (config) {
      var documentScript = document.getElementsByTagName('script')[0];
      var scriptElement = document.createElement('script');

      if (config.id) {
        scriptElement.setAttribute('id', config.id);
      }

      if ($.isFunction(config.onerror)) {
        scriptElement.onerror = config.onerror;
      }

      if ($.isFunction(config.onload)) {
        scriptElement.onload = config.onload;
      }

      if (config.defer) {
        scriptElement.defer = true;
      }

      scriptElement.type = 'text/javascript';
      scriptElement.src = config.src;

      documentScript.parentNode.insertBefore(scriptElement, documentScript);
    };

    var _getInstance = function () {
      return _recWidget();
    };

    return {
      name: 'recWidgetService',
      __protected__: {
        getInstance: _getInstance,
        insertWidgetLib: _insertWidgetLib
      }
    };
  });
