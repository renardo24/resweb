<!DOCTYPE html>
<html lang="en-us">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>How to Counteract the 'What Else' Mindset</title>
    <meta name="keywords" content="productivity, what else, Lifehacker">
    <meta name="news_keywords" content="productivity, what else, Lifehacker">
    <meta name="publish-date" content="2018-03-14T20:30:00.012Z">
    <meta name="description"
        content="For many of us, checking boxes on a to-do list is a never ending cycle, the key way we measure our productivity and self-worth. And we’re constantly adding new things before we’ve checked off the old.">
    <meta property="og:title" content="How to Counteract the 'What Else' Mindset">
    <meta property="og:type" content="article">
    <meta name="author" content="Olivier Renard">
    <meta property="og:url" content="https://lifehacker.com/how-to-counteract-the-what-else-mindset-1823772012">
    <meta property="og:description"
        content="For many of us, checking boxes on a to-do list is a never ending cycle, the key way we measure our productivity and self-worth. And we’re constantly adding new things before we’ve checked off the old.">
    <meta name="p:domain_verify" content="840a336e8dd86ab9351047db2c5bd445">
</head>

<body>
    <header>
        <h1 class="sc-1efpnfq-0 jlSocs">How to Counteract the 'What Else' Mindset</h1>
    </header>

    <div><a href="https://lifehacker.com/author/aliciaadamczyk">Alicia Adamczyk</a></div>
    </div>
    <div><time datetime="2018-03-14T16:30:00-04:00"><a
                href="https://lifehacker.com/how-to-counteract-the-what-else-mindset-1823772012">3/14/18
                4:30PM</a></time></div>
    <p>For
        many of us, checking boxes on a to-do list is a never ending cycle, the
        key way we measure our productivity and self-worth. And we’re
        constantly adding new things before we’ve checked off the old.<br></p>
    <p>Think
        about it: When was the last time you stopped to really savor a work
        victory or life accomplishment? Rather than acknowledging what you’ve
        accomplished, you’re always thinking about what’s next. <em>What else</em> could you be doing?</p>
    <p>That
        mentality can cause a whole bunch of problems in our lives. “This is,
        quite frankly, a crappy way to live your life,” writes Jocelyn K. Glei, a
        writer and host of the <span><a href="http://hurryslowly.co/" target="_blank" rel="noopener noreferrer">Hurry
                Slowly</a></span> podcast.
        “Because adopting a ‘what else?’ mindset is a recipe for making yourself feel like nothing is ever enough.”</p>
    <p>How
        does this happen? You’re likely familiar. Glei writes that it’s
        “emblematic of a toxic state of mind that’s starting to become quite
        commonplace” thanks to the proliferation of technology and social media
        that makes it so easy to compare yourself to anyone and everyone.
        Facebook, Instagram, Tinder and thousands of other websites give us
        insight into other peoples’ lives and a steady stream of information and
        content that makes asking “what else” second nature. <br></p>
    <blockquote>
        <p>You
            could witness the most precious moment of someone’s life, or read a
            news story about a horrifying and terribly sad event, or complete one of
            the most impressive accomplishments of your creative career… And
            somehow it’s never enough.</p>
    </blockquote>

    <p>Another
        tip: Make two to-do lists. One can be a “brain dump” list that holds
        everything you need to accomplish in the next month, and the other one
        can be your daily list, that’s much more manageable. Try to order tasks
        based on the amount of energy they require. “Based on the circadian
        rhythms that we all follow as human beings, most people are at their
        cognitive peak between about 9am-12pm,” she writes. “So your order of
        operations matters.” Put the task that requires the most energy and
        focus at the top of your list. </p>
    <p>And
        make them as specific as possible. For example, instead of putting
        simply “Write afternoon article” on my list, I’d break it down further
        into: get idea approved, write intro, find photo, etc. </p>
    <h3 class="sc-1bwb26k-1 fvCjqJ" id="h792339"><a class="js_header-anchor" id=""></a>Schedule “White Space” in Your
        Calendar</h3>
    <p>In design, white space is <span><a
                href="https://www.interaction-design.org/literature/article/the-power-of-white-space" target="_blank"
                rel="noopener noreferrer">used intentionally</a></span> between elements to add balance and
        organization. Glei recommends applying that concept to your daily life.</p>
    <p>“We need white space in our daily lives just as much as we need it in our designs
        because the concept carries over,” <span><a href="http://jkglei.com/white-space/" target="_blank"
                rel="noopener noreferrer">writes Glei</a></span>.
        “If our lives are over-cluttered and over-booked, we can’t focus
        properly on anything. What’s more, this way of working actually shrinks
        our ability to think creatively.”</p>
    <p>Here are some examples of white space you could add to your life to refocus: </p>
    <ul data-type="List" data-style="Bullet" class="sc-1lmbno3-0 dpuHif">
        <li><span><a href="https://lifehacker.com/learn-how-to-do-nothing-with-the-dutch-concept-of-nikse-1822310051">Letting
                    your mind wander</a></span></li>
        <li>Going for a walk<br></li>
        <li>Working out</li>
        <li>Napping</li>
        <li>Journaling<br></li>
        <li>Meditating</li>
    </ul>
    <p>Think about scheduling some time to just <em>do nothing </em>and enjoy the white
        space. </p>
    <h3 class="sc-1bwb26k-1 fvCjqJ" id="h792340"><a class="js_header-anchor" id=""></a>Create a “Stop Doing” List</h3>
    <p>Based on <span><a href="https://www.jimcollins.com/article_topics/articles/best-new-years.html" target="_blank"
                rel="noopener noreferrer">this post by Jim Collins</a></span>, Glei recommends creating a “stop doing”
        list to complement your “to do” list. Writes Collins:</p>
    <blockquote data-type="BlockQuote" class="sc-8hxd3p-0 ehyFRB">
        <p>Suppose
            you woke up tomorrow and received two phone calls. The first phone call
            tells you that you have inherited $20 million, no strings attached. The
            second tells you that you have an incurable and terminal disease, and
            you have no more than 10 years to live. What would you do differently,
            and, in particular, what would you stop doing?</p>
        <p>That
            assignment became a turning point in my life, and the “stop doing” list
            became an enduring cornerstone of my annual New Year resolutions — a
            mechanism for disciplined thought about how to allocate the most
            precious of all resources: time.﻿<br></p>
    </blockquote>
    <p>So,
        when trying to focus, ask yourself: What can you stop doing while
        working? Checking your email every five minutes? Scheduling meetings in
        the morning or eating at your desk , <span><a href="http://jkglei.com/getting-things-done/" target="_blank"
                rel="noopener noreferrer">like
                Glei</a></span>? There are likely a few habits you can break. </p>
    <h3 class="sc-1bwb26k-1 fvCjqJ" id="h792341"><a class="js_header-anchor" id=""></a>Reflect on What You’ve Already
        Achieved</h3>
    <p>Glei cites a study from the <span><a href="https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2414478"
                target="_blank" rel="noopener noreferrer">Harvard Business School</a></span>
        that posits reflecting on your accomplishments can actually be more
        helpful to your productivity and work than constantly doing more and new
        tasks. And if you think about it, that makes a lot of sense. How much
        do we <em>really</em> accomplish when we’re focused on busywork?</p>
    <p>This
        happens because it allows us to increase our belief in our own
        capacity, and increases our understanding of the task at hand. We learn
        about ourselves and our work, and like any other lesson, it only sticks
        if we take time to study and think about it. This self-reflection isn’t
        intuitive; it needs to be scheduled into our lives.</p>
    <p>“We
        have to celebrate, appreciate, and analyze our past performances, so
        that we can synthesize what we’ve learned and apply that knowledge to
        take it up a notch next time,” writes Glei.</p>
    <p>So,
        focus less on “what else” and give yourself a break. Celebrate what
        you’ve already accomplished and how far you’ve come. You deserve it.</p>

</html>