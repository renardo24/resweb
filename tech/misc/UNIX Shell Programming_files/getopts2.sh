#!/bin/sh
#
# Usage:
#
#	getopts2.sh [-P string] [-h] [file1 file2 ...]
#
# Example runs:
#
#	getopts2.sh -h -Pxerox file1 file2
#	getopts2.sh -hPxerox file1 file2
#
# Will print out the options and file names given
#

# Initialize our variables so we don't inherit values
# from the environment
opt_P=''
opt_h=''

# Parse the command-line options
while getopts 'P:h' option
do
	case "$option" in
	"P")	opt_P="$OPTARG"
		;;
	"h")	opt_h="1"
		;;
	?)	echo "getopts2.sh: Bad option specified...quitting"
		exit 1
		;;
	esac
done

shift `expr $OPTIND - 1`

if [ "$opt_P" != "" ]
then
	echo "Option P used with argument '$opt_P'"
fi

if [ "$opt_h" != "" ]
then
	echo "Option h used"
fi

if [ "$*" != "" ]
then
	echo "Remaining command-line:"
	for arg in "$@"
	do
		echo "	$arg"
	done
fi

