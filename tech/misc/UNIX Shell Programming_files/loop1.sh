#!/bin/sh
# Execute ls and wc on each of several files
# File names listed explicitly
for filename in simple.sh variables.sh
do
	echo "Variable filename is set to $filename..."
	ls -l $filename
	wc -l $filename
done
