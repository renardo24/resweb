#!/bin/sh
# Perform a global search and replace on each of several files
# File names listed explicitly
for text_file in sdsc.txt nlanr.txt
do
	echo "Editing file $text_file"
	sed -e 's/application/APPLICATION/g' $text_file > temp
	mv -f temp $text_file
done
