#!/bin/sh
# Generate some useful info for
# use at the start of the day
date
cal
last $USER | head -6
