#!/bin/sh
# An example with the case statement
# Reads a command from the user and processes it
echo "Enter your command (who, list, or cal)"
read command
case "$command" in
	who)
		echo "Running who..."
		who
		;;
	list)
		echo "Running ls..."
		ls
		;;
	cal)
		echo "Running cal..."
		cal
		;;
	*)
		echo "Bad command, your choices are: who, list, or cal"
		;;
esac
exit 0
