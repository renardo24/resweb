#!/bin/sh
# Illustrates use of a while loop to read a file
cat while2.data |   \
while read line
do
	echo "Found line: $line"
done

