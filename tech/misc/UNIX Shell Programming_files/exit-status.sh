#!/bin/sh
# Experiment with command exit status
echo "The next command should fail and return a status greater than zero"
ls /nosuchdirectory
echo "Status is $? from command: ls /nosuchdirectory"
echo "The next command should succeed and return a status equal to zero"
ls /tmp
echo "Status is $? from command: ls /tmp"
