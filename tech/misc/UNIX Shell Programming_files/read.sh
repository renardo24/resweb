#!/bin/sh
# Shows how to read a line from stdin
echo "Would you like to exit this script now?"
read answer
if [ "$answer" = y ]
then
	echo "Exiting..."
	exit 0
fi
