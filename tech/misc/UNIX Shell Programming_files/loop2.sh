#!/bin/sh
# Execute ls and wc on each of several files
# File names listed using file name wildcards
for filename in *.sh
do
	echo "Variable filename is set to $filename..."
	ls -l $filename
	wc -l $filename
done
