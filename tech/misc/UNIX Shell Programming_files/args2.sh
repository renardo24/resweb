#!/bin/sh
# Loop over the command-line arguments
# Execute with
#	sh args2.sh simple.sh variables.sh
for filename in "$@"
do
	echo "Examining file $filename"
	wc -l $filename
done
