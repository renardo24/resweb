function setStyle(style)
{
	if (document.getElementsByTagName) {
		var styles = document.getElementsByTagName('link');
		var n = 0;
		
		for (i = 0; i < styles.length; i++)
		{
			var title = styles[i].getAttribute("title");
			if (title && title != style) {
				styles[i].disabled = true;
			} else if (title && title == style){
				styles[i].disabled = false;
				n = 1;
			}
		}
		// ensure that at least one style is enabled
		if ( n == 0) styles[0].disabled = false;
		     
		var time = new Date();
		time.setTime(time.getTime() + 31536000000);
		document.cookie = 'gr_style=' + escape(style) + '; expires=' + time.toGMTString() + '; path=/';
	}
}

function setup() {
	var cooki = document.cookie.split(";");
	var name = "gr_style=";
	var styles = "";

	for(i = 0; i < cooki.length; i++) {
    	var c = cooki[i];
       
    	while(c.charAt(0) == " ") c = c.substring(1,c.length);
    	
	  	if (c.indexOf(name) == 0) styles = c.substring(name.length, c.length); 
 	}
 	
 	if (styles == "") { styles = "cyan"; }
	
	setStyle('red');
	setStyle(styles);
}

document.onload = setup();

/*
     FILE ARCHIVED ON 14:54:56 Aug 19, 2006 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 15:05:28 May 15, 2020.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 4372.701 (3)
  PetaboxLoader3.resolve: 1058.145 (2)
  exclusion.robots: 0.133
  exclusion.robots.policy: 0.124
  captures_list: 4915.566
  CDXLines.iter: 11.619 (3)
  RedisCDXSource: 527.84
  PetaboxLoader3.datanode: 2907.442 (4)
  load_resource: 480.5
  esindex: 0.013
*/