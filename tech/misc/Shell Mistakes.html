<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- saved from url=(0055)https://www.greenend.org.uk/rjk/tech/shellmistakes.html -->
<html class="fa-events-icons-ready"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Shell Mistakes</title>
    <link rel="StyleSheet" type="text/css" href="./Shell Mistakes_files/rjk.css">
    <meta name="description" content="Notes on programming in Bourne Shell">
    <meta name="category" content="reference">
    <meta name="keywords" content="sh bourne shell bash mistakes errors unix linux">
  <link href="./Shell Mistakes_files/840a321d95.css" media="all" rel="stylesheet"></head>
  <body>
    <h1>Shell Mistakes</h1>

    <ul>
      <li><a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#redundantcat">1. Redundant cat</a>
      </li><li><a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#redundantls">2. Redundant ls</a>
      </li><li><a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#missingquote">3. Missing Quoting</a>
      </li><li><a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#errorhandling">4. Error Handling</a>
	
      </li><li><a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#othermaterial">Appendix A: Other Material</a>
    </li></ul>

    <hr>

    <h2>0. Introduction</h2>

    <p>Bourne shell is a useful scripting language but it turns out
    that there are a number of easy mistakes to make when using it,
    whether interactively or in a script.  This file attempts to
    describe some of them, demonstrate that they are indeed mistakes,
    and show how to avoid them.</p>

    <h3>0.1 Assumptions</h3>

    <p>Basic familiarity with UNIX and Bourne shell is assumed: the
    reader will probably have written simple shell scripts already,
    and if not will at least know what the idea of a script is.  This
    isn't a complete shell tutorial.</p>

    <h3>0.2 Typographical Conventions</h3>

    <p>Examples are displayed boxed:</p>

    <div class="boxed"><pre class="example">Example</pre></div>

    <p>In transcripts of shell sessions, user input is displayed
    <kbd>specially</kbd>, to distinguish it from the output of the
    computer:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>echo wibble</kbd>
wibble</pre></div>

    <p>This will only work if your browser supports style sheets.  The
    distinction between user input and computer output will usually be
    reasonably obvious from context, however.</p>

    <hr>

    <h2><a name="redundantcat">1. Redundant cat</a></h2>

    <p>It seems to be extremely common to use "cat" to feed input into
    some filter.  For example:</p>

    <div class="boxed"><pre class="example">cat file | grep searchstring</pre></div>

    <p>The effect of this is to create a pipe, executing "cat file"
    with its output redirected to the pipe and "grep searchstring"
    with its input redirected from the pipe.  But you can eliminate
    the entire invocation of "cat" by just redirecting grep's input to
    come from the file:</p>

    <div class="boxed"><pre class="example">grep searchstring &lt; file</pre></div>

    <p>Not only does this avoid the time-wasting invocation of "cat",
    but (depending on the program you are running) it can sometimes
    make the process more efficient still by giving the program more
    direct access to the file.  (If performance is really vital than
    you should test the various possibilities rather than speculating
    that one is faster than another, though.)</p>

    <p>If what you want is to have the input file at the start rather
    than the end, you can do that too:</p>

    <div class="boxed"><pre class="example">&lt; file grep searchstring</pre></div>

    <p>In this case you can also just pass the name of the file to
    grep:

    </p><div class="boxed"><pre class="example">grep searchstring file</pre></div>

    <p>In other cases that might change the behaviour, though.</p>

    <h3>1.1 Exceptions</h3>

    <p>Very occasionally it does make sense to put in the "cat": if
    the process that reads the input behaves differently depending on
    whether it is a regular file, a pipe, a terminal, etc, then it
    might be that turning it into a pipe via the "cat" command makes
    sense.</p>

    <p>If you think that use of cat like this boils down to a question
    of style, read the section <a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#errorhandling">error
    handling</a>, with particular reference to the <a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#pipeerrors">remarks regarding pipelines</a>.</p>

    <hr>

    <h2><a name="redundantls">2. Redundant ls</a></h2>

    <p>A similar case is the redundant invocation of "ls".  This is
    usually in order to get a list of filenames, for example:

    </p><div class="boxed"><pre class="example">for f in `ls *.html`; do
  process "$f"
done</pre></div>

    <p>There are at least two mistakes here.  First, if any of the
    files have spaces in the name, then each word of the filename will
    be processed as a separate argument.  Watch:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>touch "a file with a space in the name"</kbd>
sfere$ <kbd>for x in `ls *`; do echo "[$x]"; done</kbd>
[a]
[file]
[with]
[a]
[space]
[in]
[the]
[name]</pre></div>

    <p>Secondly if any of the files are directories then you will get
    the contents of the directories instead of the name of the
    directory.  Like this:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>mkdir somedir</kbd>
sfere$ <kbd>touch somedir/1</kbd>
sfere$ <kbd>touch somedir/2</kbd>
sfere$ <kbd>touch 1</kbd>
sfere$ <kbd>for x in `ls s*`; do echo "[$x]"; done</kbd>
[1]
[2]</pre></div>

    <p>Notes:

    </p><ul>
      <li>"somedir" isn't listed, though it matches the pattern
      </li><li>"1" is listed, though it doesn't match the pattern
      </li><li>"2" is listed, though not only does it not match the pattern but
      there isn't even a file called "2" in the current directory.
    </li></ul>

    <p>The problems this causes might be more clearly illustrated by
    an example which actually tries to do something with the
    files:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>for x in `ls s*`; do ls -l "$x"; done</kbd>
-rw-r--r--    1 richard  richard         0 Apr 10 19:48 1
ls: 2: No such file or directory</pre></div>
    

    <p>Sometimes you know that none of the files have spaces in the
    name; and sometimes you know that there are no subdirectories
    matching your pattern.  In such cases, these problems wouldn't
    arise.</p>

    <p>However, even then, there's a quicker and easier way to achieve
    the desired effect, which is to take advantage of the fact that
    "for" actually knows about filename patterns anyway:</p>

    <div class="boxed"><pre class="example">for f in *.html; do
  process "$f"
done</pre></div>

    <h3>2.1 Excluding Directories</h3>

    <p>This still includes any subdirectories that match the pattern.
    If the contents of the loop should apply to directories anyway, or
    produce a harmless error message when passed a directory, then
    this may not be a problem.  If you need to separately eliminate
    them then you must test each one explicitly:</p>

    <div class="boxed"><pre class="example">for f in *.html; do
  if test ! -d "$f"; then
    process "$f"
  fi
done</pre></div>

    <p>A variant on this is to instead of ignoring directories, to
    ignore anything that isn't a regular (i.e. ordinary) file:</p>

    <div class="boxed"><pre class="example">for f in *.html; do
  if test -f "$f"; then
    process "$f"
  fi
done</pre></div>

    <hr>

    <h2><a name="missingquote">3. Missing Quoting</a></h2>

    <p>Shell's quoting syntax is arcane but unfortunately to write
    reliable scripts it's necessary to use it fully.  The problem is
    quite simple: if you don't use quoting in the right place, then
    your script won't behave properly.  For example:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>touch "a file with spaces in the name"</kbd>
sfere$ <kbd>for x in *; do ls $x; done</kbd>
ls: a: No such file or directory
ls: file: No such file or directory
ls: with: No such file or directory
ls: spaces: No such file or directory
ls: in: No such file or directory
ls: the: No such file or directory
ls: name: No such file or directory</pre></div>

    <p>The answer in this case is fairly simple: quote variable
    expansions in "double quotes":</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>for x in *; do ls "$x"; done</kbd>
a file with spaces in the name</pre></div>

    <p>The full rules for quoting apply
    both to scripts and to commands entered interactively.</p>

    <p>In the examples below we'll create a file called "test file",
    with a space in the name, using various different quoting styles.
    If you use no quoting at all, then the command used to create the
    file will see two separate arguments, thus creating two files
    rather than one file with a space in the name:</p>
      
    <div class="boxed"><pre class="example">sfere$ <kbd>touch test file</kbd>
sfere$ <kbd>ls -l</kbd>
total 0
-rw-r--r--    1 richard  richard         0 Apr  7 13:43 file
-rw-r--r--    1 richard  richard         0 Apr  7 13:43 test</pre></div>

    <p>(They appear in reverse order because ls sorts filenames
    lexically by default.)</p>

    <ul>
      <li><a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#backslashquote">Backslashes</a>
      </li><li><a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#singlequote">Single Quotes</a>
      </li><li><a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#doublequote">Double Quotes</a>
      </li><li><a href="https://www.greenend.org.uk/rjk/tech/shellmistakes.html#dollarat">Special Behaviour Of $@</a>
    </li></ul>

    <h3><a name="backslashquote">3.1 Backslashes</a></h3>
    
    <p>Outside of any other form of quoting, a backslash ("\") may be
    used to quote any character except newline.  In this
    example, the space character is preceded with a backslash to make
    the shell treat "test file" as a single argument, creating a
    file with a space in the name:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>touch test\ file</kbd>
sfere$ <kbd>ls -l </kbd>
total 0
-rw-r--r--    1 richard  richard         0 Apr  7 13:11 test file</pre></div>

    <p>Sometimes the quoted character is referred to as being
    "escaped" and the backslash as an "escape character".</p>

    <p>The one exception is that a backslash followed by a newline
    doesn't quote the newline: it removes it (and the backslash)
    entirely, allowing a command to span multiple lines.  For
    example:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>echo foo\</kbd>
&gt; <kbd>bar</kbd>
foobar
sfere$ </pre></div>
    
    <p>Notes:

    </p><ul>
      <li>The "&gt; " is output from the shell, not user input
      </li><li>The user didn't type a space after the newline
      </li><li>No space was seen by the shell after the newline - "foobar"
      is a single word
    </li></ul>

    <p>If you have reconfigured your shell's prompting, then you may
    get something different from "&gt; ".</p>

    <h3><a name="singlequote">3.2 Single Quotes</a></h3>

    <p>Any character in a string contained in 'single quotes' has no
    special meaning.  (The exception of course is that the single
    quote character itself cannot appear in such a string.)  For
    example:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>touch 'test file'</kbd>
sfere$ <kbd>ls -l </kbd>
total 0
-rw-r--r--    1 richard  richard         0 Apr  7 13:39 test file</pre></div>

    <p>It is perfectly permissible to quote newlines this way; for
    example:

    </p><div class="boxed"><pre class="example">sfere$ <kbd>echo 'first line</kbd>
&gt; <kbd>second line'</kbd>
first line
second line</pre></div>

    <p>Here, the echo command sees a single argument, which happens to
    have a newline in it.</p>

    <h3><a name="doublequote">3.3 Double Quotes</a></h3>

    <p>"Double quotes" are the most flexible kind of quoting.  Certain
    characters, like space and tab, which have special meaning outside
    of quotes lose that meaning inside double quotes.  However other
    characters retain their special meaning, or change slightly.</p>

    <p>The simple example of creating our test file looks like this:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>touch "test file"</kbd>
sfere$ <kbd>ls -l</kbd>
total 0
-rw-r--r--    1 richard  richard         0 Apr  7 13:46 test file</pre></div>

    <p>The set of characters that retain special meaning inside double
    quotes is as follows:</p>

    <table>
      <tbody><tr>
	<th>Character
	</th><th>Description
      </th></tr><tr>
	<td valign="top" align="center">$
	</td><td>The dollar symbol retains its special meaning of starting
	an expansion or substitution of some sort.
      </td></tr><tr>
	<td valign="top" align="center">`
	</td><td>The backquote retains its special meaning
	of executing a command and substituting the output into the
	string. 
      </td></tr><tr>
	<td valign="top" align="center">\
	</td><td>The backslash can be used to quote only certain
	characters; in particular $, `, ", \ and newline.  If it is
	used before any other character then it is <em>not</em> removed.
    </td></tr></tbody></table>
    
    <h3><a name="dollarat">3.4 Special Behaviour of $@</a></h3>

    <p>$@ is a built-in shell variable that contains the names of
    arguments to the shell script.  Superficially it is identical to
    $*, which also contains the argument values.  However it behaves
    slightly differently when quoted.</p>

    <p>For the examples here we'll assume the arguments to the script
    are "arg 1" and "arg 2" - both containing spaces.  You can
    simulate this interactively with the "set" command:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>set "arg 1" "arg 2"</kbd></pre></div>

    <p>If you just use $* to get at the parameters, then you fall
    victim to quoting problems; the arguments get split into separate
    words at each space character:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>for x in $*; do echo "[$x]"; done</kbd>
[arg]
[1]
[arg]
[2]</pre></div>

    <p>If you try to quote $* then the result is no better - now you
    get all the arguments combined into a single word:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>for x in "$*"; do  echo "[$x]"; done</kbd>
[arg 1 arg 2]</pre></div>

    <p>(The character that is used to join each word together is taken
    from the first character of the value of $IFS.  This is worth
    knowing for cases where what you want is all of the script
    arguments joined together into one.)</p>

    <p>An unquoted $@ is no different to $*.  However, if you
    double-quote it then you get the desired result:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>for x in "$@"; do  echo "[$x]"; done</kbd>
[arg 1]
[arg 2]</pre></div>

    <p>Notes:

    </p><ul>
      <li>This behaviour only occurs if $@ appears inside double
      quotes

      </li><li>If there are no arguments to the script, then "$@" expands
      to no words at all, rather than to a single empty string.

      </li><li>If there are other characters in the string, then they will
      be joined to the first or last word resulting from the expansion
      of $@.

      </li><li>In modern shells, the <tt>in "$@"</tt> is redundant; however
      Solaris /bin/sh still requires it.

    </li></ul>

    <p>As an example of the final point:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>for x in "S $@ E"; do  echo "[$x]"; done</kbd>
[S arg 1]
[arg 2 E]</pre></div>
    
    <h3><a name="quotenest">3.5 Nested Quotes</a></h3>

    <p>Quotes don't nest as such: putting a single quote character
    inside a double-quoted string doesn't start a single-quoted
    string, it just stands for itself.</p>

    <p>However, if you use shell commands such as "eval" and "trap"
    then you will find that you have to write quotes within quotes
    sometimes; it's very easy to get confused if you do this, as the
    inner quoting characters themselves must be quoted properly.</p>

    <p>The approach to take is decide exactly what the command you
    want eval or trap to execute is; then considering it as a string
    of characters, rather than as a command, add quoting as necessary
    to remove special meaning from all special characters.</p>

    <p>Consider this command:</p>

    <div class="boxed"><pre class="example">echo \\ `echo spong`</pre></div>

    <p>This already contains an escaped backslash; and it contains a
    command substitution.  If you run it directly it produces the
    following output:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>echo \\ `echo spong`</kbd>
\ spong</pre></div>

    <p>If you want to run it via eval (which evaluates its argument as
    if it were a single shell command) then it must be quoted
    appropriately.  Any of the three quoting styles can be used in
    this case.</p>

    <p>If you decide to use backslashes then you must quote at least
    the characters that get special interpretation.  This doesn't work
    if there are any newlines in the string.  If in doubt as to
    whether any given character needs quoting, quote it.</p>

    <p>In this case, we'll put a backslash before each space,
    backslash and backquote, and not bother with the other
    characters.  This still applies "inside" the backquotes: at the
    point we are applying the quoting, this is just a sequence of
    characters, not a shell command at all - it only gets interpreted
    later.  Each backslash has to be escaped separately for the same
    reason, causing the number of backslashes required to produce a
    single backslash in the eventual output to double up from two to
    four.  The result is not very readable:</p>

    <div class="boxed"><pre class="example">eval echo\ \\\\\ \`echo\ spong\`</pre></div>

    <p>If you choose double quotes, then any backslash, dollar, backquote or
    double quote in the string must be escaped with a backslash; all
    other characters remain unchanged.  Newlines are acceptable.</p>

    <div class="boxed"><pre class="example">eval "echo \\\\ \`echo spong\`"</pre></div>

    <p>That's a bit better: we don't have to worry about quoting
    spaces now.  However, the quoted backslashes still double up to
    four.  In a more complex example it would rapidly become
    unmanageable.</p>

    <p>If you opt to use single quotes, then you can include newlines
    in the string, but not single quotes themselves.  In this example,
    this is quite obviously the winner, being far more readable than
    the alternatives:</p>

    <div class="boxed"><pre class="example">eval 'echo \\ `echo spong`'</pre></div>

    <p>Double quotes are the only way to get completely general
    quoting, however.</p>

    <h3><a name="changequoting">3.6 Changing Quoting</a></h3>

    <p>It's possible to change quoting style in the middle of a word,
    for example:</p>
    
    <div class="boxed"><pre class="example">sfere$ <kbd>echo "foo bar"wibble</kbd>
foo barwibble</pre></div>

    <p>Although this is occasionally useful, it isn't the clearest of
    syntaxes.

    </p><h3><a name="quotesummary">3.7 Quoting Summary</a></h3>

    <table>
      <tbody><tr>
	<th>Quote
	</th><th>Exceptions
	</th><th>Notes
      </th></tr><tr>
	<td valign="top" align="center">\
	</td><td valign="top">newline
	</td><td>Escapes any single character other than newline; behaves
	differently inside quotes.  \ + newline is removed entirely,
	to spread a command over multiple lines.
      </td></tr><tr>
	<td valign="top" align="center">'
	</td><td valign="top">'
	</td><td>Quote any character except ' itself.  Newlines stand for
	themselves.
      </td></tr><tr>
	<td valign="top" align="center">"
	</td><td valign="top">", $, `, \
	</td><td>Directly quote any character except " itself, plus $, `
	and \; all of these must be escaped.  Newlines stand for
	themselves.
      </td></tr><tr>
	<td valign="top" align="center">\ when inside ""
	</td><td valign="top">all but ", $, `, \
	</td><td>Escapes $, `, \, ".  \ + newline is removed entirely,
	spreading a string across multiple lines.  For any other
	character \ has no effect (and is not removed).
    </td></tr></tbody></table>

    <hr>

    <h2><a name="errorhandling">4. Error Handling</a></h2>

    <p>Things go wrong.  Users mis-spell filenames; discs get full;
    programs crash.  It's hard to consider every possibility, but
    scripts are often written to completely ignore all errors,
    sometimes leading to very destructive failure modes.</p>

    <p>For example, imagine a script which modifies a file
    "in-place".</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>ls -l t</kbd>
-rwxr-xr-x    1 richard  richard        98 Apr  7 15:16 t
sfere$ <kbd>cat t</kbd>
#! /bin/sh
for file; do
  sed &lt; "$file" &gt; "$file.tmp" 's/foo/bar/g'
  mv "$file.tmp" "$file"
done</pre></div>

    <p>This works fine under normal circumstances:

</p><div class="boxed"><pre class="example">sfere$ <kbd>cat input</kbd>
wibble foo spong
sfere$ <kbd>./t input</kbd>
sfere$ <kbd>cat input</kbd>
wibble bar spong</pre></div>

    <p>But what happens if we run out of disc space?  Well, the sed
    command will get as far as reading in data, but when it tries to
    write it out it will get a write error.  If it got as far as
    opening the output file, then the result will be that input.tmp
    will now be an empty file - and so when we invoke mv, we'll
    replace the input file with a completely empty file.</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>cat input</kbd>
wibble foo spong
sfere$ <kbd>./t input</kbd>
sed: Couldn't close {standard output}: No space left on device
sfere$ <kbd>cat input</kbd>
sfere$ </pre></div>

    <p>We know something's gone wrong, because sed produced an error
    message, but our script has destroyed our input file!  Hopefully
    we have a backup, but it would be better not to have lost it in
    the first place.</p>

    <p>(The error message appears to refer to closing the file rather
    than writing to it - this is because the I/O library used by sed
    buffers output internally before actually writing it to disc, so
    the last few bytes of the output only get written out when the
    output file is closed at the end of the program.)</p>

    <p>What can we do about this?  Ideally we'd like sed to tell us
    when it fails to write some or all of its output and prevent the
    mv command from occurring.  And sed does tell us when it fails: conventionally when a
    program succeeds, it exits with status zero; if any error
    occurs it exits with some nonzero status.</p>

    <p>After executing a command, the shell puts the exit status into
    the built-in variable $?.  We can check that this is zero after
    each command.  For example:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>cat t</kbd>
#! /bin/sh
for file; do
  sed &lt; "$file" &gt; "$file.tmp" 's/foo/bar/g'
  if [ $? = 0 ]; then
    mv "$file.tmp" "$file"
  fi
done
sfere$ <kbd>./t input</kbd>
sed: Couldn't close {standard output}: No space left on device
sfere$ <kbd>echo $?</kbd>
0
sfere$ <kbd>cat input</kbd>
wibble foo spong
sfere$ <kbd>ls</kbd>
input  input.tmp  t</pre></div>

    <p>Problems with this script are:</p>

    <ul>
      <li>You have to remember to check the exit status of every
      command that might go wrong.  For a small script, this may not
      seem like much work; for a large script it could triple the line
      count.

      </li><li>It doesn't make this script itself exit with a nonzero
      status (so some further script that invoked it wouldn't be
      easily able to tell whether it succeeded or failed).

      </li><li>It leaves the temporary file lying around.

    </li></ul>

    <p>A quick solution to the first of these problems can be
    found in the -e shell option.  If this option is turned on then if
    any command exits with nonzero status, the entire script is
    immediately terminated.  For example:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>cat t</kbd>
#! /bin/sh
set -e
for file; do
  sed &lt; "$file" &gt; "$file.tmp" 's/foo/bar/g'
  mv "$file.tmp" "$file"
done
sfere$ <kbd>./t input</kbd>
sed: Couldn't close {standard output}: No space left on device
sfere$ <kbd>echo $?</kbd>
4
sfere$ <kbd>cat input</kbd>
wibble foo spong
</pre></div>

    <p>Notes:

    </p><ul>
      <li>"set -e" doesn't in fact make the script exit if
      <em>any</em> command exits with nonzero status: the exceptions
      are basically those that make while, until, if, &amp;&amp;, ||
      and ! work.  (But see below.)

      </li><li>Not all commands exit with a status 0 for success and
      something else for errors.  For example, GNU diff returns a
      status of 0 to mean no differences, 1 for differences found and
      other values to indicate an error.  In such cases you must
      explicitly check the value $? to determine whether the command
      succeeded.

      </li><li>Sometimes it isn't appropriate to exit on a failure.
      Often such cases can be best handled with "if"; sometimes it is
      more appropriate to temporarily turn off "-e" (with <span class="userinput">set +e</span>) for a few commands, then turn it
      back on again.
    
    </li></ul>

    <h3>4.1 Cleaning Up After Errors</h3>

    <p>That temporary file is still lying around, wasting disc space
    and looking untidy; although here it's just one file, it's not
    unheard of to see thousands of redundant temporary files lying
    around, and this can become a serious problem.</p>

    <p>One way to ensure it gets cleaned up is to use the "trap"
    command, as follows:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>cat t</kbd>
#! /bin/sh
set -e
for file; do
  trap 'rm -f "$file.tmp"' 0
  sed &lt; "$file" &gt; "$file.tmp" 's/foo/bar/g'
  mv "$file.tmp" "$file"
done
sfere$ <kbd>./t input</kbd>
sed: Couldn't close {standard output}: No space left on device
sfere$ <kbd>echo $?</kbd>
4
sfere$ <kbd>ls</kbd>
input  t</pre></div>

    <p>Note the nesting of quotes in the "trap" command.  This causes
    the exit handler to be set without any expansion going on - so it
    will be the exact characters <b>rm -f "$file.tmp"</b>.  The quotes
    and expansion of $file will be processed only when the handler is
    executed.</p>
      
    <p>The reason for this is that if we expanded the filename early,
    then we'd have to escape any spaces in the filename, which is
    messy to get right and (as shown) not actually necessary.</p>

    <p>Also, note that the "trap" command appears inside the loop
    rather than outside the loop: otherwise, when the script is invoked
    with no arguments, the file ".tmp" will be deleted.</p>

    <p>An approach that avoids the need for nested quoting is to use a
    shell function to do the cleanup:</p>

    <div class="boxed"><pre class="example">#! /bin/sh
set -e

cleanup() {
  rm -f "$file.tmp"
}

for file; do
  trap cleanup 0
  sed &lt; "$file" &gt; "$file.tmp" 's/foo/bar/g'
  mv "$file.tmp" "$file"
done</pre></div>
      
    <p>Note that some (obsolete) shells don't support functions, so
    this approach isn't guaranteed to be completely portable.</p>

    <h3><a name="pipeerrors">4.2 Pipelines</a></h3>

    <p>Unfortunately "set -e" doesn't work properly with pipelines, as
    only the exit status of the last command in a pipeline is checked
    by the shell: for all other commands, it is ignored.</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>cat u</kbd>
#! /bin/sh
set -e
true | false | true
echo "reached"
sfere$ <kbd>./u</kbd>
reached
sfere$ <kbd>echo $?</kbd>
0</pre></div>

    <p>("false" is a command which always exits with nonzero status.)</p>

    <p>There's not much that can portably be done about this, other than
    avoiding pipelines or accepting that some errors will not be
    automatically detected.  The latter approach is often appropriate
    for interactive commands, but only rarely for scripts.</p>

    <p>Pipelines can be avoided using temporary files to hold
    intermediate data (but this may be inappropriate if the
    intermediate data is large, or needs to be processed immediately)
    or perhaps by setting up named pipes between commands.</p>

    <p>GNU bash has a PIPESTATUS variable which can be used to pick up
    the exit status of each command in a pipeline:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>true|false|true</kbd>
sfere$ <kbd>echo ${PIPESTATUS[0]} ${PIPESTATUS[1]} ${PIPESTATUS[2]}</kbd>
0 1 0</pre></div>

    <p>One wrinkle that is worth knowing: if a command in a pipeline
    terminates before it has read all its input from the previous
    command, the previous command might receive the SIGPIPE signal.
    If it does not handle it then it will exit with nonzero status,
    even though the error is expected.  For example:</p>

    <div class="boxed"><pre class="example">sfere$ <kbd>find|head -1</kbd>
.
sfere$ <kbd>echo ${PIPESTATUS[0]} ${PIPESTATUS[1]}</kbd>
141 0</pre></div>

    <p>Here, everything worked as expected - find didn't get an error
    trying to read all the data, it only exited nonzero because head
    didn't read beyond the first line.  So, be careful of SIGPIPE when
    looking at exit statuses.</p>
    
    <p>Notes:</p>

    <ul>
      <li>The value 141 above is actually 128+13, where 13 is the
      numeric value of SIGPIPE and 128 is the value the shell adds to
      signal numbers to indicate that a process died due to a signal
      rather than exiting "normally".  SIGPIPE might be different from
      13 on other systems.</li>

      <li>If you use bash features in a script, you must start it with
      "#! /bin/bash" (or whatever is appropriate if bash is installed
      somewhere else) rather than "#! /bin/sh".</li>

      <li>Some programs install a handler for SIGPIPE, or ignore it.
      This can make it hard to distinguish between "safe" failure due
      to SIGPIPE and fatal errors due to some other problem.</li>

    </ul>

    <h3>4.3 Reliable Writes</h3>

    <p>In the example above we wrote to a temporary file and renamed
    it into place, as we still needed the input file around while we
    were generating the output file.  This isn't always the case, but
    the approach remains a good one.</p>

    <p>Imagine a script which rewrites your resolv.conf when your
    computer gets a new DHCP lease.  The part of it that rewrites the
    file might look something like this:</p>

    <div class="boxed"><pre class="example">echo "search $mydomain" &gt; /etc/resolv.conf
echo "nameserver $nameservers" &gt;&gt; /etc/resolv.conf</pre></div>

    <p>But consider what happens if something goes wrong while this is
    running - if the disc fills up, if the power fails, etc.  Your
    resolv.conf will be be empty or incomplete and whatever
    information about nameserver addresses, etc, was in the old
    version will have been destroyed.</p>

    <p>A common approach is to take a backup copy before writing the
    new one:</p>

    <div class="boxed"><pre class="example">cp /etc/resolv.conf /etc/resolv.conf.bak</pre></div>

    <p>Although this means the information can be easily recovered, we
    can actually do better by writing to a temporary file in the same
    directory and then renaming it into place:</p>

    <div class="boxed"><pre class="example">echo "search $mydomain" &gt; /etc/resolv.conf.tmp
echo "nameserver $nameservers" &gt;&gt; /etc/resolv.conf.tmp
mv /etc/resolv.conf.tmp /etc/resolv.conf</pre></div>
    
    <p>Now resolv.conf always has either the old data or the new data
    in it - there is not a single moment when it is incomplete.</p>

    <p>The reason that the file should be in the same directory is
    because that's the easiest way to guarantee that it's on the same
    filesystem; this is useful because mv across filesystems is in fact implemented as a
    copy, which has all the disadvantages of the original version.</p>

    <p>There is a downside to this approach: if the file to be written
    is a symlink then it will be replaced by an ordinary file.  Also,
    if (through hard links) the file has more than one name in the
    file system, then the connection between the names will be
    broken.  The former problem can be solved by following the link to
    find the "real" name of the file, then latter is not really
    possible to work around - if this is a problem for you then the
    next best bet is to take a backup of the file before rewriting it
    and put up with it being incomplete if something does go wrong.</p>

    <hr>

    <h2><a name="othermaterial">Appendix A: Other Material</a></h2>

    <h3>A.1 References</h3>
    
    <p>The <a href="http://www.opengroup.org/onlinepubs/7908799/">Single UNIX
    Specification (http://www.opengroup.org/onlinepubs/7908799/)</a>
    has a section on <a href="http://www.opengroup.org/onlinepubs/007908799/xcu/chap2.html">Shell
    Command Language
    (http://www.opengroup.org/onlinepubs/007908799/xcu/chap2.html)</a>.</p>

    <p>Most UNIX systems will respond to the command "man sh" with the
    documentation for their shell.</p>

    <p>At <a href="http://language.perl.com/ppt/v7/sh.1">http://language.perl.com/ppt/v7/sh.1</a>
    is a copy of the man page for the UNIX v7 implementation of
    /bin/sh.</p>

    <h3>A.2 Faking Errors</h3>

    <p>I had to fake up some of the errors above.  The technique I
    used to do this was to create a shared object to simulate the
    error and insert it into the running program with LD_PRELOAD.  The
    source file is <a href="https://www.greenend.org.uk/rjk/tech/werror.c">werror.c</a>; it works on my
    Debian system.</p>

    <hr>

    <h2>Administrivia</h2><a href="https://www.greenend.org.uk/rjk/misc/copying.html">Copyright © 2001 Richard Kettlewell</a>.<p></p>

    <p><a href="https://www.greenend.org.uk/rjk/">RJK</a> | <a href="https://www.greenend.org.uk/rjk/contents.html">Contents</a></p>
  

<!--
Local variables:
mode: sgml
sgml-indent-data:t
End:
-->
</body></html>